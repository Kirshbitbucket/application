#!/usr/bin/env groovy
import hudson.model.*
import hudson.EnvVars
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import java.net.URL

node{
    stage('pull code'){
	git 'https://github.com/Githubkiruba/DevOpsClassCodes.git'
    }
    stage ("ABK-Dockerbuild"){
               sh label: '', script: '''rm -rf dockerbuild
mkdir dockerbuild
cd dockerbuild

cp /var/lib/jenkins/workspace/addbook_package/target/addressbook.war .

cat << \'EOF\' > Dockerfile
FROM tomcat
ADD addressbook.war /usr/local/tomcat/webapps/addressbook.war
CMD ["catalina.sh", "run"]
EOF

sudo docker build . -t addressbookimage:$BUILD_NUMBER'''
        }
    stage ("ABK-Container"){
        sh label: '', script: 'sudo docker run -itd --name myaddressbookimage$BUILD_NUMBER -p 16007:8080 addressbookimage:$BUILD_NUMBER'
    }
}
